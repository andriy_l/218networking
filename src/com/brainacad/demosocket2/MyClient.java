package com.brainacad.demosocket2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Random;

/**
 * Created by andriy on 01.04.17.
 */
public class MyClient implements Runnable {
    String[] students = {"Andriy","Valentyn", "Olexandr", "Vitaliy"};
    @Override
    public void run() {
        try (Socket socket = new Socket("localhost", 1234);
             ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())){

            Student student = new Student(
                    students[new Random().nextInt(students.length-1)],
                    "Java course",
                    new Random().nextInt(100)
            );
            oos.writeObject(student);
                System.out.println( ois.readUTF());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
