package com.brainacad.demosocket2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class MyServer implements Runnable {
    private List<Student> users = new ArrayList<>();

    private ServerSocket serverSocket;
    private static int port = 1234;

    private void createStudents() {
        int id = 1;
        users.add(new Student("Petro", "Basics", id++));
        users.add(new Student("Pedro", "PHP", id++));
        users.add(new Student("Vasyl", "FrontEnd", id++));
        users.add(new Student("Andriy", "Java", id++));
        users.add(new Student("Vitaliy", "C#", id++));
        users.add(new Student("Ivan", "F#", id++));
    }


    @Override
    public void run() {
        createStudents();
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                new Thread(new ConnectionHandler(serverSocket.accept())).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ConnectionHandler implements Runnable {
        private Socket socket;

        public ConnectionHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream())) {
                Student student = (Student) ois.readObject();
//                if (student != null) {
//                    System.out.println("Student" + student.getName() + "readed successfully");
//                }
                boolean found = false;
                ListIterator<Student> usersIterator = users.listIterator();
                while (usersIterator.hasNext()) {
                    Student student1 = usersIterator.next();
                    if(student1.equals(student)){
                        found = true;
                    }
                }
                if(found){
                    oos.writeUTF(student.getName() + " Access Granted");
                } else {
                    oos.writeUTF(student.getName() + " Access Denied!");
                }


            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}