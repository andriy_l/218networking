package com.brainacad.demosocket2;

/**
 * Created by andriy on 01.04.17.
 */
public class Main {
    public static void main(String[] args) {
        Thread server = new Thread(new MyServer());
        int clientNum = 20;

        server.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < clientNum; i++) {
            new Thread(new MyClient()).start();
        }

    }
}
