package com.brainacad.demosocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by andriy on 01.04.17.
 */
public class MyClient implements Runnable {
    @Override
    public void run() {
        try (Socket socket = new Socket("localhost", 1234);
             ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())){

            Student student = new Student("Vasyl", "Java course", 123);
            oos.writeObject(student);
                System.out.println((Student) ois.readObject());

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
