package com.brainacad.demosocket;

/**
 * Created by andriy on 01.04.17.
 */
public class Main {
    public static void main(String[] args) {
        Thread server = new Thread(new MyServer());
        Thread client = new Thread(new MyClient());
        server.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        client.start();
    }
}
