package com.brainacad.demosocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer implements Runnable{
    private ServerSocket serverSocket;
    private static int port = 1234;

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                Socket s = serverSocket.accept();
                ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
                ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
                Student student = (Student) ois.readObject();
                if(student != null) {
                    System.out.println("Student readed successfully");
                }
                String studentName = "Connected " + student.getName();
                student.setName(studentName);
                oos.writeObject(student);
                System.out.println("Student sended to a client");
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
