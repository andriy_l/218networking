package com.brainacad.oop.testnest1.classsample;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by andriy on 25.03.17.
 */
public class Cln {
    public static void main(String[] args) {
        try {
            byte[] ip = new byte[]{(byte) 192, (byte) 168, (byte) 2, (byte) 175};
            InetAddress inetAddress = Inet4Address.getByAddress(ip);
            int port = 9999;
            Socket s = new Socket("localhost", port);
            PrintWriter pw = new PrintWriter(s.getOutputStream());
            Scanner scanner = new Scanner(s.getInputStream());
            String str = Arrays.toString(ip) + " hello \n";
            System.out.println(str);
            pw.write(str);
            pw.flush();
            while (scanner.hasNext()) {
                System.out.println(scanner.nextLine());
            }
        }catch(IOException e){
         e.printStackTrace();
            }
    }
}
