package com.brainacad.oop.testnest1.classsample;


import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List<Socket> connections = new ArrayList<>();
        try(ServerSocket ss = new ServerSocket(9999);){
            while (true) {
                Socket s = ss.accept();
                connections.add(s);
                SocketAddress ia = s.getRemoteSocketAddress();
                Thread t = new Thread(() -> {

                    try {
                        FileWriter fw = new FileWriter("log.txt", true);
                        Scanner scanner = new Scanner(s.getInputStream());
                        while (scanner.hasNext()) {
                            fw.write(ia + " " + scanner.nextLine()+"\n");
                            fw.flush();
//                fw.close();
                            for (Socket socket : connections) {
                                OutputStream os = socket.getOutputStream();
                                PrintWriter pw = new PrintWriter(os);
                                String fileInStr
                                        = new String(
                                        Files.readAllBytes(Paths.get("log.txt")),
                                        "UTF-8");
                                pw.write(fileInStr);
                                pw.flush();

                            }
                        }
                        } catch(IOException e){
                            e.printStackTrace();
                        }

                });
                t.start();

            }

        }catch (IOException ioe){
                ioe.printStackTrace();
        }

    }
}
