package com.brainacad.oop.testurlconnect;

import java.io.IOException;
import java.net.*;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by andriy on 3/26/17.
 */
public class Main {
    public static void main(String[] args) throws IOException,URISyntaxException {
        try {

//            URL url = new URL("http://mainacad.com/ua/");
            URL url = new URL("https://www.google.com.ua:443/search?q=java+vs+.net&ie=utf-8&oe=utf-8&gws_rd=cr&ei=zPnXWO7dBImasgHuy72YDQ");
            URI uri = url.toURI();
            // tel:+38099999999 // URName
//            System.out.println(
//            "authority: " + uri.getAuthority() + "\n" +
//            "host: " + uri.getHost() + "\n" +
//            "port: " + uri.getPort() + "\n" +
//            "path: " + uri.getPath() + "\n" +
//            "query: " + uri.getQuery() + "\n" +
//            "fragment: " + uri.getFragment() + "\n"
//            );
//
                URLConnection urlConnection = url.openConnection();
                urlConnection.setConnectTimeout(10_000);
                // we set request properties
                urlConnection.setRequestProperty("Accept-Language", "ua");
                urlConnection.setRequestProperty("Accept-Charset", "utf-8");
                urlConnection.setRequestProperty("Cookie", "andriy_l");
                urlConnection.addRequestProperty("Cookie", "user");
                // connect
                urlConnection.connect();
                // get response
                // response fields started from 1 to N,
        // if requested field is 0 or gt N - null
//            System.out.println("field No 4: "
//                    +urlConnection.getHeaderFieldKey(4)
//                    +" has value: "+urlConnection.getHeaderField(4) + "\n");

            Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
            int i = 1;
            for(String headerKey: headerFields.keySet()){
                System.out.println(headerKey + ": " + urlConnection.getHeaderField(i++));

            }



        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }

}
}
