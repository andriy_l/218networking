package com.brainacad.urlconnection;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by andriy on 01.04.17.
 */
public class URLConnectionDemo {
    public static void main(String[] args) {
        String get1 = "нові книги";
        String get2 = "new books";
        try {
            String get1enc = URLEncoder.encode(get1,"UTF-8");
            System.out.println("http://i.ua/action?req="+get1enc);
            String get2enc = URLEncoder.encode(get2,"UTF-8");
            System.out.println("http://i.ua/action?req=" + get2enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        URL url = null;
        URLConnection urlConnection = null;
        try {
            url = new URL("https://google.com");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            urlConnection = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        urlConnection.setRequestProperty("Content-type:","text/html");
        urlConnection.addRequestProperty("Content-type:","text/csv");

    }
}
