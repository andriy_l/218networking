package samples.tcpsockets01;

import java.io.*;
import java.net.*;
public class SmallClientSocket {
    public static void main(String[] args) {

        try (Socket socket = new Socket("localhost", 8030);
            InputStreamReader inputStream = new InputStreamReader(socket.getInputStream());
            BufferedReader br = new BufferedReader(inputStream)){
            String message    = br.readLine();
            System.out.println(message);
        } catch (IOException e) {
            System.err.println("err: " + e);
        }
    }
}
