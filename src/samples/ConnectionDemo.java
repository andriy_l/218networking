package samples;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
public class ConnectionDemo {
    public static void main(String[] args) {
        String urlName = "http://www.oracle.com";
               URL url2 = null;
            try{
      url2 = new URL("file:///home/andriy/Documents/brainacad-java/p2/2_1_8-networking/javax.mail-1.5.6.jar");
    } catch (MalformedURLException e) {
        e.printStackTrace();
    }

            URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{url2});
//            urlClassLoader.loadClass();
        System.out.println(urlClassLoader.getURLs());
        // ftp:
        // http:
        // https:
        // file:
        // jar:
        int timeout = 10_000_000;
        URL url;

        try {
            url = new URL(urlName);
            final URLConnection connection = url.openConnection();
// set timeout connection
            connection.setConnectTimeout(timeout);
            System.out.println(urlName + " content type: "
                    + connection.getContentType());
// continue
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
