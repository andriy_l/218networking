package samples;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class InetAddressDemo {
    public static void main(String[] args) {
        InetAddress currentIP = null;
        InetAddress oracle = null;
        InetAddress[] google = null;


        InetAddress googleDNS = null;
        Inet4Address inet4Address;
        Inet6Address inet6Address;
        try {
            currentIP = InetAddress.getLocalHost();
// вивід IP-адреси локального комп'ютера
            System.out.println("МY IP -> " + currentIP.getHostAddress());
            oracle = InetAddress.getByName("www.oracle.com");
// вивід IP-адреси  www.oracle.com
            System.out.println("ORACLE -> " + oracle.getHostAddress());

            google = InetAddress.getAllByName("www.google.com");

            System.out.println("Google Hosts/IPs " + Arrays.toString(google));
            //Google Hosts/IPs
            // [www.google.com/173.194.113.208,
            // www.google.com/173.194.113.212,
            // www.google.com/173.194.113.210,
            // www.google.com/173.194.113.209,
            // www.google.com/173.194.113.211,
            // www.google.com/2a00:1450:4001:811:0:0:0:2004]
        } catch (UnknownHostException e) {
// якщо не може знайти IP
            System.err.println("адресa недоступнa " + e);
        }

        byte[] ipGoogleDNS = {(byte)8, (byte)8, (byte)8, (byte)8};
        byte[] ipOracle = {(byte)104, (byte)81, (byte)108, (byte)164}; //104.81.108.164
        byte[] ipMy = {(byte)127, (byte)0, (byte)0, (byte)1}; //127.0.0.1
        try {
            InetAddress addressG = InetAddress.getByAddress(ipGoogleDNS);
            InetAddress addressO = InetAddress.getByAddress("www.oracle.com", ipOracle);
            InetAddress addressL = InetAddress.getByAddress("localhost", ipMy);
            System.out.println("G: " + addressG.getHostName() + "-> connections:" + addressG.isReachable(10000));
            System.out.println("O: " + addressO.getHostName() + "-> connections:" + addressO.isReachable(10000));
            System.out.println("My: " + addressL.getHostName() + "-> connections:" + addressL.isReachable(10000));
        } catch (UnknownHostException e) {
            System.err.println("адресa недоступнa " + e);
        } catch (IOException e) {
            System.err.println("stream error " + e);
        }


    }
}