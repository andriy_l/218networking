/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package samples.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import sun.util.calendar.Gregorian;

/**
 * @author andriy
 */
public class NBUCurrency {
    public static void main (String [] args) {


        System.out.println(  getCurrency("XAU") + '\u20B4');
        System.out.println(  getCurrency("USD") + '\u20B4');
        System.out.println(  getCurrency("GBP") + '\u20B4');
    }
    /**
     * Data is available if we have working day.
     * In other case get data on previous working day.
     * http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=20160407";
     *  
     * @see class Calendar - How to work with Date
     * @param currency XAU - Gold
     * @return
     * @throws MalformedURLException
     * @throws IOException 
     */
    public static double getCurrency (String currencyName) { // throws MalformedURLException, IOException {
        double gbpCurrency = 0;
        String url = "http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange";
//        https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json
//        GregorianCalendar gc = new GregorianCalendar();
        
        String charset = "UTF-8"; 
        HttpURLConnection httpConnection = null;
        try {            
            httpConnection = (HttpURLConnection) new URL(url).openConnection(); 
        } catch (MalformedURLException ex) {
            Logger.getLogger(NBUCurrency.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NBUCurrency.class.getName()).log(Level.SEVERE, null, ex);
        }
        httpConnection.setRequestProperty("Accept-Charset", charset);
        InputStream response = null;
        try {
        response = httpConnection.getInputStream();
        }catch (IOException ioe) {
            Logger.getLogger(NBUCurrency.class.getName()).log(Level.SEVERE, null, ioe);
        }
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            Document document = null;
            NodeList nodeList;
            try {
                builder = factory.newDocumentBuilder();
                try{
                document = builder.parse(new InputSource(response));
                }catch (MalformedURLException mue) {
                    Logger.getLogger(NBUCurrency.class.getName()).log(Level.SEVERE, null, mue);
                }
                nodeList = document.getElementsByTagName("currency");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node currency = nodeList.item(i);
                    if (currency.getNodeType() == Node.ELEMENT_NODE) {
                        Element e = (Element) currency;
                        if ((e.getElementsByTagName("cc").item(0).getTextContent()).equals(currencyName)) {
                        gbpCurrency = Double.parseDouble(e.getElementsByTagName("rate").item(0).getTextContent());                        
                        break;
                    }
                    }
                }            
            } catch (Exception e) {
                Logger.getLogger(NBUCurrency.class.getName()).log(Level.SEVERE, null, e);
                // e.printStackTrace();
                gbpCurrency = -1;                
            }
            return gbpCurrency;
    }    
}
