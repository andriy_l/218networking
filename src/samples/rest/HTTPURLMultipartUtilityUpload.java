/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package samples.rest;

import java.io.BufferedReader;

import static java.lang.System.currentTimeMillis;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.URLConnection.guessContentTypeFromName;
import static java.text.MessageFormat.format;
import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HTTPURLMultipartUtilityUpload {

    private static final Logger log = getLogger(HTTPURLMultipartUtilityUpload.class
            .getName());

    private static final String CRLF = "\r\n";
    private static final String CHARSET = "UTF-8";

    private static final int CONNECT_TIMEOUT = 15000;
    private static final int READ_TIMEOUT = 10000;

    private HttpURLConnection connection = null;
    private OutputStream outputStream;
    private PrintWriter writer;
    private String boundary;

    // for log formatting only
    private final URL url;
    private final long start;
    private String response;

    public HTTPURLMultipartUtilityUpload(final URL url) {
        start = currentTimeMillis();
        this.url = url;

        boundary = "---------------------------" + currentTimeMillis();

        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Logger.getLogger(HTTPURLMultipartUtilityUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setReadTimeout(READ_TIMEOUT);
        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            Logger.getLogger(HTTPURLMultipartUtilityUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.setRequestProperty("Accept-Charset", CHARSET);
        connection.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + boundary);
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);

        try {
            outputStream = connection.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(HTTPURLMultipartUtilityUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            writer = new PrintWriter(new OutputStreamWriter(outputStream, CHARSET),
                    true);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HTTPURLMultipartUtilityUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addFormField(final String name, final String value) {
        writer.append("--").append(boundary).append(CRLF)
                .append("Content-Disposition: form-data; name=\"").append(name)
                .append("\"").append(CRLF)
                .append("Content-Type: text/plain; charset=").append(CHARSET)
                .append(CRLF).append(CRLF).append(value).append(CRLF);
    }

    public void addFilePart(final String fieldName, final File uploadFile)
            throws IOException {
        final String fileName = uploadFile.getName();
        writer.append("--").append(boundary).append(CRLF)
                .append("Content-Disposition: form-data; name=\"")
                .append(fieldName).append("\"; filename=\"").append(fileName)
                .append("\"").append(CRLF).append("Content-Type: ")
                .append(guessContentTypeFromName(fileName)).append(CRLF)
                .append("Content-Transfer-Encoding: binary").append(CRLF)
                .append(CRLF);

        writer.flush();
        outputStream.flush();
        final FileInputStream inputStream = new FileInputStream(uploadFile);
        try {
            final byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
        }catch (IOException io) {
            io.printStackTrace();
        }

        writer.append(CRLF);
    }

    public void addHeaderField(String name, String value) {
        writer.append(name).append(": ").append(value).append(CRLF);
    }

    public String finish() throws IOException {
        writer.append(CRLF).append("--").append(boundary).append("--")
                .append(CRLF);
        writer.close();

        int status = connection.getResponseCode();
        /*
            Reader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            for (int c; (c = in.read()) >= 0;) {
                response += ((char) c);
            }
            in.close();
            */
        final InputStream is = connection.getInputStream();
        try{
            final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            final byte[] buffer = new byte[4096];
            int bytesRead;
            if (status != HTTP_OK) {
                System.out.println(response);
                throw new IOException(format("{0} failed with HTTP status: {1}",
                        url, status));
            }
            while ((bytesRead = is.read(buffer)) != -1) {
                bytes.write(buffer, 0, bytesRead);
            }
            response = bytes.toString();

            log.log(INFO,
                    format("{0} took {1} ms", url,
                            (currentTimeMillis() - start)));

            //bytes.toByteArray();

        } finally {
            connection.disconnect();
        }
      return response;
    }
}
