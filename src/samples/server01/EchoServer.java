package samples.server01;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This program implements a simple server01 that listens to port 8189 and echoes back all client
 * input.
 * @version 1.21 2012-05-19
 * @author Cay Horstmann
 */
public class EchoServer
{
   public static void main(String[] args) throws IOException
   {
       // The security manager is a class that allows applications to implement a security policy.
       // It allows an application to determine,
       // before performing a possibly unsafe or sensitive operation,
       // what the operation is and whether it is being attempted
       // in a security context that allows the operation to be performed.
       // The application can allow or disallow the operation.
       SecurityManager security = System.getSecurityManager();


        int port = 8189;
      // establish server01 socket00
      try (ServerSocket s = new ServerSocket(port))
      {

          if (security != null) {
              System.out.println("i am allowed to listen port? " );
              security.checkPermission(new  SocketPermission("localhost:"+port,"listen"));
              security.checkListen(port);
          }
         // wait for client connection
         try (Socket incoming = s.accept())
         {
            InputStream inStream = incoming.getInputStream();
            OutputStream outStream = incoming.getOutputStream();
   
            try (Scanner in = new Scanner(inStream, "UTF-8"))
            {
               PrintWriter out = new PrintWriter(
                  new OutputStreamWriter(outStream, "UTF-8"),
                  true /* autoFlush */);
      
               out.println("Hello! Enter BYE to exit.");
      
               // echo client input
               boolean done = false;
               while (!done && in.hasNextLine())
               {
                  String line = in.nextLine();
                  out.println("Echo: " + line);
                   System.out.println("LOG: " + line);
                  if (line.trim().equals("BYE")) done = true;
               }
            }
         }
      }
   }
}