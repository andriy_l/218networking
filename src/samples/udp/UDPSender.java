package samples.udp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
public class UDPSender {
    public static void main(String[] args) {

        String fileName = "files"+File.separator+"Rooster.mp3";
        try (FileInputStream fr = new FileInputStream(new File(fileName))) {
            byte[] data = new byte[1024];
            DatagramSocket dSocket = new DatagramSocket();

            InetAddress address = InetAddress.getLocalHost();
            DatagramPacket packet;
            while (fr.read(data) != -1) {

                packet = new DatagramPacket(data, data.length, address, 8033);

                dSocket.send(packet);
            }
            System.out.println("file sended");
        } catch (UnknownHostException e) {

            e.printStackTrace();
        } catch (SocketException e) {

            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
