package samples.udp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
public class UDPRecipient {
    public static void main(String[] args) {

        File file = new File("UDPtoxic.mp3");
        System.out.println("getting data...");
        // recieving file
        acceptFile(file, 8033, 1024);
    }
    private static void acceptFile(File file, int port, int pacSize) {
        byte data[] = new byte[pacSize]; // 1

        DatagramPacket pac = new DatagramPacket(data, data.length); // 2
        try (FileOutputStream os = new FileOutputStream(file)) {
            DatagramSocket s = new DatagramSocket(port); // 3


            s.setSoTimeout(60_000);
            while (true) {
                s.receive(pac); // 4
                os.write(data);
                os.flush();
            }
        } catch (SocketTimeoutException e) {

            System.err.println("eee");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
