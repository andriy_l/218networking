package samples.urlConnection;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Created by andriy on 3/27/17.
 */
public class Base64EncodeDemo {
    public static void main(String[] args) {
        Path roosterFileMP3 = Paths.get("files","Rooster.mp3");
        System.out.println(roosterFileMP3 + " has length "
                + roosterFileMP3.toFile().length() + " in bytes");
        byte[] imageAsByteArray = null;
        try {
            imageAsByteArray = Files.readAllBytes(Paths.get("files","Rooster.mp3"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Checksum crc32OfSource = new CRC32(); // Cyclic Redundancy Check
        crc32OfSource.update(imageAsByteArray,0, imageAsByteArray.length);
        long crc32OfSourceValue = crc32OfSource.getValue();
        System.out.println("CRC32: " + crc32OfSourceValue);
        Base64.Encoder encoder = Base64.getEncoder();
        String base64encoded = encoder.encodeToString(imageAsByteArray);
        System.out.println("file as base64: \n" + base64encoded);
        try (FileWriter fw = new FileWriter(roosterFileMP3 + ".txt")) {
            fw.write(base64encoded);
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] encodedImage = null;
        try {
            encodedImage = decoder.decode(new String(Files.readAllBytes(Paths.get("files","Rooster.mp3.txt"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Checksum crc32OfResult = new CRC32();
        crc32OfResult.update(encodedImage,0, encodedImage.length);
        long crc32OfResultValue = crc32OfSource.getValue();
        System.out.println("CRC32: " + crc32OfSourceValue);
        System.out.println("written and readed arrays are " +
                (Arrays.hashCode(imageAsByteArray)==Arrays.hashCode(encodedImage)?"eqaul":"not equal"));
        System.out.println("written and readed arrays are (CRC32) " +
                (crc32OfSourceValue==crc32OfResultValue?"eqaul":"not equal"));

    }
}
