package samples.socket00;

import sun.security.x509.IPAddressName;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This program makes a socket00 connection to the atomic clock in Boulder, Colorado, and prints
 * the time that the server01 sends.
 * 
 * @version 1.21 2016-04-27
 * @author Cay Horstmann
 */
public class SocketTest
{
   public static void main(String[] args) {

//       String hostname = "time-a.timefreq.bldrdoc.gov";
       String hostname = "lib.ru";
//       int port = 13;
       int port = 80;
//       try (Socket s =
//                    new Socket(hostname,
//                            port,
//                            Inet4Address
//                                    .getByAddress(
//                                            new byte[]{
//                                                    (byte)192,
//                                                    (byte)168,
//                                                    (byte)2,
//                                                    (byte)175}),
//                            65330);
//       try (Socket s = new Socket(hostname, port, Inet4Address.getByAddress(new byte[]{(byte)10, (byte)245, (byte)62, (byte)5}), 65530);
      try (Socket s = new Socket(hostname, port);
         Scanner in = new Scanner(s.getInputStream(), "UTF-8")) {
          s.setSoTimeout(1000);
//          s.setKeepAlive(true);
//          System.out.println("Is keep-alive? " +s.getKeepAlive());
          System.out.println("local port "+s.getLocalPort());
          System.out.println("local address "+s.getLocalAddress());
          System.out.println("tcp no_delay " + s.getTcpNoDelay());
          // This option g(s)ets the type-of-service or traffic
          // class field in the IP header for a TCP or UDP socket.
          // In practice, the ToS field never saw widespread use outside
          // of US Department of Defense networks.
          // However, a great deal of experimental,
          // research, and deployment work has focused on how to make use of these eight bits
          System.out.println("traffic class (TOS) " + s.getTrafficClass());
          System.out.println("is connected? " + s.isConnected());
          System.out.println("binding state " + s.isBound());
          // to receive URG ent data
          s.setOOBInline(true);
         while (in.hasNextLine())
         {
            String line = in.nextLine();
            System.out.println(line);
         }
      }catch(UnknownHostException unknownHostException) {
          System.out.println("host is unknown");

      }catch (IOException e){
            e.printStackTrace();
      }
   }
}
